import React from 'react';

export default class HelloWorld extends React.Component {
  render() {
    return <div>{JSON.stringify(this.props)}</div>
  }
}
